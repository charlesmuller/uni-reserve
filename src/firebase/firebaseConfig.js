import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBN09_XrNm9re--dcJrKx_Y6yKT-mo9Ihc",
  authDomain: "uni-reserve.firebaseapp.com",
  projectId: "uni-reserve",
  storageBucket: "uni-reserve.appspot.com",
  messagingSenderId: "838695741527",
  appId: "1:838695741527:web:SuaChaveAqui"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;
