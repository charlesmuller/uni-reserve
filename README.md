## Iniciando aplicação
# dentro do diretório da aplicação rode:
- `docker-compose up -d`

# Endereço da aplicação via navegador:
- `http://localhost:8100/tab1`

# Endereço do banco de dados:
- `http://localhost:8080/index.php?route=/`