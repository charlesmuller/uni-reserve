FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g create-vite
RUN npm install -g @ionic/cli
RUN npm install -g vite
RUN npm install --save-dev @vitejs/plugin-legacy
RUN npm install

COPY . .

EXPOSE 8100

CMD ["ionic", "serve", "--host", "0.0.0.0"]
